/* Generated automatically. */
static const char configuration_arguments[] = "../gcc-10.2.0/configure --target= --prefix=/home/maksym/conix-core-project/Conix-Core/tools/tmp/ --disable-nls --enable-languages=c --without-headers --target= --without-headers --enable-shared --enable-threads=posix --enable-__cxa_atexit --enable-clocale=gnu --enable-gnu-unique-object --enable-linker-build-id --enable-lto --enable-plugin --with-linker-hash-style=gnu --enable-gnu-indirect-function --disable-multilib --disable-werror --enable-checking=release : (reconfigured) ../gcc-10.2.0/configure --target= --prefix=/home/maksym/conix-core-project/Conix-Core/tools/tmp/ --disable-nls --enable-languages=c --without-headers --target= --without-headers --enable-shared --enable-threads=posix --enable-__cxa_atexit --enable-clocale=gnu --enable-gnu-unique-object --enable-linker-build-id --enable-lto --enable-plugin --with-linker-hash-style=gnu --enable-gnu-indirect-function --disable-multilib --disable-werror --enable-checking=release";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" } };
